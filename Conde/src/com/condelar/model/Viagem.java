package com.condelar.model;

import java.util.Date;

public class Viagem {

	private int id;
	private String destino;
	private String localSaida;
	private Date dt;
	private boolean status;
	
	public Viagem(int id, String dest, String sai, Date dt) {
		this.id = id;
		this.destino = dest;
		this.localSaida = sai;
		this.dt = dt;
		this.status = true; 
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getDestino() {
		return this.destino;
	}
	
	public String getLocalSaida() {
		return this.localSaida;
	}
	
	public Date getData() {
		return this.dt;
	}
	
	public boolean getStatus() {
		return this.status;
	}
	
	public void setStatus(boolean stts) {
		this.status = stts;
	}

}
