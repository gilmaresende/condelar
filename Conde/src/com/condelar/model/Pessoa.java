package com.condelar.model;

import java.util.Date;

import com.condelar.constants.Sexo;
import com.condelar.util.DataConde;

public class Pessoa extends CondeModel{
	private Long identificador;
	private String nome;
	private String email;
	private String nickname;
	private String password;
	private DataConde nascimento;
	private DataConde cadastro;
	private Enum sexo;
	
	public Pessoa() {
		
	}
	
	public void cadastrar() {
		
	}

	public Long getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(DataConde nascimento) {
		this.nascimento = nascimento;
	}

	public Enum getSexo() {
		return sexo;
	}

	public void setSexo(Enum sexo) {
		this.sexo = sexo;
	}
	
	public Short getSexoShort() {
		if(sexo.equals(Sexo.MASCULINO)) {
			return 0;
		}
		else if(sexo.equals(Sexo.MASCULINO)) {
			return 1;
		}
		return 2;
	}
	
	public void setSexoShort(Short s) {
		if(s.equals(0)) {
			sexo = Sexo.MASCULINO;
		}
		else if(s.equals(1)) {
			sexo = Sexo.FEMINININO;
		}
	}
	
	
	
	

}
