package com.condelar.modelDAO;

import java.util.Date;

import com.condelar.constants.Sexo;
import com.condelar.model.Pessoa;
import com.condelar.util.DataConde;

public class PessoaDAO {

	Pessoa pessoa;
	
	public PessoaDAO() {
		// TODO Auto-generated constructor stub
	}
	
	
	@SuppressWarnings("deprecation")
	public static Pessoa getPessoaNickName(String nickname) {
		
		Pessoa p = new Pessoa();
		p.setNome("Gilmar Fabiano Lara Resende");
		p.setEmail("gilmaresende@gmail.com");
		p.setIdentificador(1l);
		p.setNascimento(new DataConde(1996,1,21));
		p.setPassword("badwolf96");
		p.setNickname(nickname);
		p.setSexo(Sexo.MASCULINO);
		
		return p;
	}
	
	public static String cadastrar(Pessoa p) {
		return "Sucesso";
	}
	
	public static String alterar(Pessoa p) {
		return "Erro";
	}
	
	public static Boolean verificarEmailJaCadastrado(String email) {
		return false;
	}
	
	public static Boolean verificarNickName(String nick) {
		return false;
	}
}
