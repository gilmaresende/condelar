package com.condelar.constants;

public enum StateView {
	
	LIMPO_EDITAVEL,
	LIMPO_BLOQUEADO,
	COM_INFORMACAO_EDITAVEL,
	COM_INFORMACAO_BLOQUEADO
}
