package com.condelar.vision;

import java.awt.Color;

import javax.swing.JFrame;

import com.condelar.vision.base.CondeFrame;

public class CondeRunFrame extends CondeFrame{
	
	
	PessoaPanel panelPessoa = new PessoaPanel();
	
	public CondeRunFrame() {
		this.setName("Condelar");
		this.getContentPane().setBackground(Color.red);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600,500);
		this.centralizarComponente();
	}
	
	public void start() {
		
		this.add(panelPessoa);
		this.setVisible(true);
		
	}	
}

