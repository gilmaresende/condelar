package com.condelar.vision.auxiliar;

import java.awt.Event;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import com.condelar.vision.PessoaPanel;
import com.condelar.vision.base.CondeFrame;
import com.condelar.vision.base.CondePanel;
import com.condelar.vision.base.CondePanelBase;;

public class InPutIndentificador extends CondeFrame {
	
	
	InPutIndentificadorPanel inp;
	CondePanelBase pessoaPanel;
	
	public InPutIndentificador(CondePanelBase p) {
		
		this.setName("NickName");
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
		this.setSize(400,128);
		this.centralizarComponente();
		pessoaPanel = p;
		inp = new InPutIndentificadorPanel(this);
		start();
	}
	
	public void start() {
		this.add(inp);
		this.setVisible(true);
	}
	
	public void submitAndClose(String s) {
		this.pessoaPanel.pesquisarString(s);
		this.dispose();
	}
	
	public void close() {
		
		this.dispose();
	}
	
}

class InPutIndentificadorPanel extends CondePanel {

    private javax.swing.JButton ButtonBuscar;
    private javax.swing.JButton buttonCancelar;
    private javax.swing.JLabel labelNickName;
    private javax.swing.JTextField textNickname;
    
    InPutIndentificador frame;
   
    /**
     * Creates new form InPutIndentificador
     */
    public InPutIndentificadorPanel(InPutIndentificador frame ) {
    	this.frame = frame;
        initComponents();
    }
    
    private void initComponents() {

        labelNickName = new javax.swing.JLabel();
        labelNickName.setText("NickName");
        
        textNickname = new javax.swing.JTextField();
        textNickname.setText("");
        textNickname.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == Event.ENTER) {
					frame.submitAndClose(textNickname.getText());
				}
			}
		});

        ButtonBuscar = new javax.swing.JButton();
        ButtonBuscar.setText("Buscar");
        ButtonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBuscarActionPerformed(evt);
            }
        });
        
        buttonCancelar = new javax.swing.JButton();
        buttonCancelar.setText("Cancelar");
        buttonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelarActionPerformed(evt);
			}
        });

        configurarLayout();
        
        
        
    }  
    
    private void configurarLayout(){
    	javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(67, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonCancelar)
                    .addComponent(labelNickName, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textNickname, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ButtonBuscar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(67, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNickName)
                    .addComponent(textNickname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonCancelar)
                    .addComponent(ButtonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );
    }

    private void buttonCancelarActionPerformed(java.awt.event.ActionEvent evt) {                                               
        this.frame.close();
    } 
    
    private void ButtonBuscarActionPerformed(java.awt.event.ActionEvent evt) {                                             
    	this.frame.submitAndClose(textNickname.getText());
    }      
}
