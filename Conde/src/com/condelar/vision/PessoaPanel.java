package com.condelar.vision;

import javax.swing.JOptionPane;

import com.condelar.constants.ConstantsConde;
import com.condelar.constants.Sexo;
import com.condelar.constants.StateView;
import com.condelar.model.CondeModel;
import com.condelar.model.Pessoa;
import com.condelar.modelDAO.PessoaDAO;
import com.condelar.util.DataConde;
import com.condelar.util.Validacao;
import com.condelar.vision.auxiliar.InPutIndentificador;
import com.condelar.vision.base.CondePanelBase;

/*
 * 
 * @auto Gilmar Fabiano Lara Resende
 * @version 1.0.0
 */
public class PessoaPanel extends CondePanelBase {


	private javax.swing.JLabel labelEmail;
	private javax.swing.JLabel labelConfirPassword;
	private javax.swing.JLabel labelNickName;
	private javax.swing.JLabel labelNome;
	private javax.swing.JLabel labelPassword;
	private javax.swing.JLabel labelSexo;
	private javax.swing.JLabel labelnascimento;
	private javax.swing.JLabel labelIdentificador;
	
	private javax.swing.JLabel textIdentificador;
	
	private javax.swing.JTextField textNickName;
	private javax.swing.JTextField textNome;
	private javax.swing.JTextField textEmail;
	
	private javax.swing.JPasswordField textPassword;
	private javax.swing.JPasswordField textPasswordConfir;
	
	private javax.swing.JButton buttonNovo;
	private javax.swing.JButton ButtonAlterar;
    private javax.swing.JButton buttonBuscar;
    private javax.swing.JButton buttonSalvar;
    private javax.swing.JButton buttonCancelar;
    
	private javax.swing.JRadioButton radioFeminino;
	private javax.swing.JRadioButton radioMasculino;
	
	private javax.swing.JComboBox<String> selctMes;
	private javax.swing.JComboBox<String> selectAno;
	private javax.swing.JComboBox<String> selectDia;
	
	private javax.swing.JInternalFrame jInternalFrame1;
	
	private StateView estado;
	
	private Pessoa vo;
	
	/*
	 * Construtor
	 */
	public PessoaPanel() {
		initComponents();
	}

	/*
	 * instanciar componentes
	 */
	private void initComponents() {

		labelIdentificador = new javax.swing.JLabel();
		labelIdentificador.setText("ID.");
		textIdentificador = new javax.swing.JLabel();
		
		
		labelNome = new javax.swing.JLabel();
		labelNome.setText("Nome");
		textNome = new javax.swing.JTextField();
		textNome.setEnabled(false);
		
		labelEmail = new javax.swing.JLabel();
		labelEmail.setText("e-mail");
		textEmail = new javax.swing.JTextField();
		textEmail.setEnabled(false);
		
		labelNickName = new javax.swing.JLabel();
		labelNickName.setText("NickName");
		textNickName = new javax.swing.JTextField();
		textNickName.setEnabled(false);
		labelPassword = new javax.swing.JLabel();
		labelPassword.setText("Password");
		textPassword = new javax.swing.JPasswordField();
		
		textPassword.setEnabled(false);
		labelConfirPassword = new javax.swing.JLabel();
		labelConfirPassword.setText("Confirmar Password");
		textPasswordConfir = new javax.swing.JPasswordField();
		textPasswordConfir.setEnabled(false);
		
		labelnascimento = new javax.swing.JLabel();
		labelnascimento.setText("Data Nascimento");
		
		selectAno = new javax.swing.JComboBox<>();
		selectAno.setEnabled(false);
		selectAno.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				releaseMonth(evt);
			}
		});
		
		selctMes = new javax.swing.JComboBox<>();
		selctMes.setEnabled(false);
		selctMes.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				releaseDay(evt);
			}
		});
		
		selectDia = new javax.swing.JComboBox<>();
		selectDia.setEnabled(false);
		
		labelSexo = new javax.swing.JLabel();
		labelSexo.setText("Sexo");
		
		radioMasculino = new javax.swing.JRadioButton();
		radioMasculino.setText("Masculino");
		radioMasculino.setEnabled(false);
		radioMasculino.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				radioMasculinoActionPerformed(evt);
			}
		});
				
		radioFeminino = new javax.swing.JRadioButton();
		radioFeminino.setText("Femenino");
		radioFeminino.setEnabled(false);
		radioFeminino.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				radioFemininoActionPerformed(evt);
			}
		});
		
		buttonNovo = new javax.swing.JButton();
		buttonNovo.setText("Novo");
		buttonNovo.setMaximumSize(new java.awt.Dimension(85, 23));
		buttonNovo.setMinimumSize(new java.awt.Dimension(85, 23));
		buttonNovo.setPreferredSize(new java.awt.Dimension(85, 23));
		buttonNovo.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				novoActionPerformed(evt);
			}
		});
		
		buttonBuscar = new javax.swing.JButton();
		buttonBuscar.setText("Buscar");
        buttonBuscar.setMaximumSize(new java.awt.Dimension(85, 23));
        buttonBuscar.setMinimumSize(new java.awt.Dimension(85, 23));
        buttonBuscar.setPreferredSize(new java.awt.Dimension(85, 23));
        buttonBuscar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buscarActionPerformed(evt);
			}
		});

        buttonSalvar = new javax.swing.JButton();
        buttonSalvar.setText("Salvar");
        buttonSalvar.setMaximumSize(new java.awt.Dimension(85, 23));
        buttonSalvar.setMinimumSize(new java.awt.Dimension(85, 23));
        buttonSalvar.setPreferredSize(new java.awt.Dimension(85, 23));
        buttonSalvar.setEnabled(false);
        buttonSalvar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cadastrarActionPerformed(evt);
			}
		});
        
        ButtonAlterar = new javax.swing.JButton();
        ButtonAlterar.setText("Alterar");
        ButtonAlterar.setMaximumSize(new java.awt.Dimension(85, 23));
        ButtonAlterar.setMinimumSize(new java.awt.Dimension(85, 23));
        ButtonAlterar.setPreferredSize(new java.awt.Dimension(85, 23));
        ButtonAlterar.setEnabled(false);
        ButtonAlterar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				alterarActionPerformed(evt);
			}
		});
        
        buttonCancelar = new javax.swing.JButton();
        buttonCancelar.setText("Cancelar");
        buttonCancelar.setMaximumSize(new java.awt.Dimension(85, 23));
        buttonCancelar.setMinimumSize(new java.awt.Dimension(85, 23));
        buttonCancelar.setPreferredSize(new java.awt.Dimension(85, 23));
        buttonCancelar.setEnabled(false);
        buttonCancelar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelarrActionPerformed(evt);
			}
		});
		
		jInternalFrame1 = new javax.swing.JInternalFrame();
		jInternalFrame1.setVisible(true);
		javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
		jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
		jInternalFrame1Layout.setHorizontalGroup(jInternalFrame1Layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 0, Short.MAX_VALUE));
		jInternalFrame1Layout.setVerticalGroup(jInternalFrame1Layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 0, Short.MAX_VALUE));
		
		iniciaClear();
		modelarLayout();
		
	}
	
	/*
	 * Limpa todos os campos da tela
	 */
	private void iniciaClear() {
		
		textIdentificador.setText("");
		textNome.setText("");
		textEmail.setText("");
		textNickName.setText("");
		textPassword.setText("");
		textPasswordConfir.setText("");
		selectAno.setModel(new javax.swing.DefaultComboBoxModel<>(DataConde.getYearListString(1900, DataConde.getYearToday())));
		selctMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mes" }));
		selectDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dia" }));
		radioMasculino.setSelected(false);
		radioFeminino.setSelected(false);
		estado = StateView.LIMPO_BLOQUEADO;
	}

	/*
	 * Se o radio masculino for marcado, desmarca o feminino
	 */
	private void radioMasculinoActionPerformed(java.awt.event.ActionEvent evt) {

		radioFeminino.setSelected(false);
	}

	/*
	 * Se o radio feminino for marcado, desmarca o masculino
	 */
	private void radioFemininoActionPerformed(java.awt.event.ActionEvent evt) {

		radioMasculino.setSelected(false);
	}

	/*
	 * verifica se � o ano ataul, se for seta ate o mes atual, caso contrario sera 12
	 */
	private void releaseMonth(java.awt.event.ActionEvent evt) {
		
		int ano = DataConde.getYearWIndex(selectAno.getSelectedIndex());
		
		if (ano > 0) {
			
			if(ano==DataConde.getYearToday()) {
				selctMes.setModel(new javax.swing.DefaultComboBoxModel<>(ConstantsConde.getNumericList(DataConde.getMonthToday(), "Mes")));
			}
			else {
				selctMes.setModel(new javax.swing.DefaultComboBoxModel<>(ConstantsConde.getNumericList(12, "Mes")));
			}
			selctMes.setEnabled(true);
		} else {
			selctMes.setEnabled(false);
			selectDia.setEnabled(false);
		}
	}
	/*
	 * Seta quantidade de dias que o mes pode ter, 28, 30 ou 31. Se for o mes atual seta ate o dia de hoje.
	 */
	private void releaseDay(java.awt.event.ActionEvent evt) {
		int mes = selctMes.getSelectedIndex();
		int ano = DataConde.getYearWIndex(selectAno.getSelectedIndex());
		if (mes > 0) {
			if(ano==DataConde.getYearToday()&&mes==DataConde.getMonthToday()) {
				selectDia.setModel(new javax.swing.DefaultComboBoxModel<>(ConstantsConde.getNumericList(DataConde.getDayToday(), "Dia")));
			}
			else {
				selectDia.setModel(new javax.swing.DefaultComboBoxModel<>(ConstantsConde.getNumericList(DataConde.quantidadeDiasMes(mes, 1), "Dia")));//este caso especifico pode ser passado qualquer ano que n�o seja bissexto.
			}
			selectDia.setEnabled(true);
		} else {
			selectDia.setEnabled(false);
		}
	}

	/*
	 * Valida as informa��es para salvar uma pessoa.
	 */
	@SuppressWarnings("deprecation")
	private void cadastrarActionPerformed(java.awt.event.ActionEvent evt) {
		Boolean ok = true;
		vo = new Pessoa();
		String aviso;
		Boolean vEmail = false;
		Boolean vNick = false;
		if (textNome.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Nome � Obrigat�rio!!!");
			ok = false;
		} else {
			vo.setNome(textNome.getText());
		}

		if (textNickName.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "NickName � Obrigat�rio!!!");
			ok = false;
		} else {
			vNick = PessoaDAO.verificarNickName(textNickName.getText());
			if(vNick) {
				JOptionPane.showMessageDialog(null, "NickName j� Existente!!!");
				ok = false;
			}
			else {
				vo.setNickname(textNickName.getText());
			}
		}

		if (textEmail.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "e-mail � Obrigat�rio!!!");
			ok = false;
		} else {
			if (Validacao.validarEmail(textEmail.getText())) {
				vEmail = PessoaDAO.verificarEmailJaCadastrado(textEmail.getText());
				if(vEmail) {
					JOptionPane.showMessageDialog(null, "E-mail j� cadastrado!!!");
					ok = false;
				}
				else {
					vo.setEmail(textEmail.getText());
				}
			} else {
				JOptionPane.showMessageDialog(null, "E-mail Invalido");
				ok = false;
			}
		}

		if (textPassword.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Senha n�o pode estar em branco!!!");
			ok = false;
		} else if (!textPassword.getText().equals(textPasswordConfir.getText())) {
			JOptionPane.showMessageDialog(null, "Senhas n�o Conhecidem!!!");
			ok = false;
		} else {
			vo.setPassword(textPassword.getText());
		}

		if (radioMasculino.isSelected()) {
			vo.setSexo(Sexo.MASCULINO);
		} else if (radioFeminino.isSelected()) {
			vo.setSexo(Sexo.FEMINININO);
		} else {
			JOptionPane.showMessageDialog(null, "Selecione o Sexo!!!");
			ok = false;
		}
		int dia = selectDia.getSelectedIndex();
		int mes = selctMes.getSelectedIndex();
		int ano = DataConde.getYear(selectAno.getSelectedIndex());
		if (dia > 0 && mes > 0 && ano > 0) {
			vo.setNascimento(new DataConde(ano, mes, dia));
		} else {
			JOptionPane.showMessageDialog(null, "Informe seu nascimento!!!");
			ok = false;
		}

		if (ok) {
			if(textIdentificador.getText().equals("")) {
				aviso = PessoaDAO.cadastrar(vo);
				block();
				estado = StateView.COM_INFORMACAO_BLOQUEADO;
				ButtonAlterar.setEnabled(true);
				pesquisarString(vo.getNickname());
			}
			else {
				aviso = PessoaDAO.alterar(vo);
				block();
				estado = StateView.COM_INFORMACAO_BLOQUEADO;
				ButtonAlterar.setEnabled(true);
			}
			JOptionPane.showMessageDialog(null, aviso);
			
		}
	}
	
	/*
	 * Habilita os campos para criar um novo cadastro de pessoa.
	 */
	private void novoActionPerformed(java.awt.event.ActionEvent evt) {
		modoEditavel();
		iniciaClear();
		textPassword.setEnabled(true);
		textPasswordConfir.setEnabled(true);
		estado = StateView.LIMPO_EDITAVEL;
	}
	
	/*
	 * Cancela a��o que estiver selecionada
	 */
	private void cancelarrActionPerformed(java.awt.event.ActionEvent evt) {
		if(estado == StateView.COM_INFORMACAO_EDITAVEL) {
			preencher(vo);
			block();
			ButtonAlterar.setEnabled(true);
		}
		else if(estado == StateView.LIMPO_EDITAVEL) {
			iniciaClear();
			block();
		}
	}
	
	
	/*
	 * Abilita editar para serem editados
	 */
	private void modoEditavel() {
		textNome.setEnabled(true);
		textEmail.setEnabled(true);
		textNickName.setEnabled(true);
		selectAno.setEnabled(true);
		selctMes.setEnabled(false);
		radioMasculino.setEnabled(true);
		radioFeminino.setEnabled(true);
		buttonNovo.setEnabled(false);
		buttonBuscar.setEnabled(false);
		buttonSalvar.setEnabled(true);
		ButtonAlterar.setEnabled(false);
		buttonCancelar.setEnabled(true);
		
	}
	
	/*
	 * Permite alterar dados na tela
	 */
	private void alterarActionPerformed(java.awt.event.ActionEvent evt) {
		modoEditavel();
		textPassword.setEnabled(false);
		textPasswordConfir.setEnabled(false);
		estado = StateView.COM_INFORMACAO_EDITAVEL;
	}
	
	/*
	 * Abre uma tela de entrar com nickname
	 */
	private void buscarActionPerformed(java.awt.event.ActionEvent evt) {
		InPutIndentificador in = new InPutIndentificador(this);
	}

	/* 
	 * Este Metodo bloqueia os campos editaveis na tela.
	 */
	private void block() {
		textNome.setEnabled(false);
		textEmail.setEnabled(false);
		textNickName.setEnabled(false);
		textPassword.setEnabled(false);
		textPasswordConfir.setEnabled(false);
		selectAno.setEnabled(false);
		selctMes.setEnabled(false);
		selectDia.setEnabled(false);
		radioMasculino.setEnabled(false);
		radioFeminino.setEnabled(false);
		buttonNovo.setEnabled(true);
		buttonBuscar.setEnabled(true);
		buttonSalvar.setEnabled(false);
		ButtonAlterar.setEnabled(false);
		buttonCancelar.setEnabled(false);
	}
	
	/*
	 * Este metodo preenche as informa��es na tela
	 */
	@Override
	public void preencher(CondeModel conde) {
		vo = (Pessoa) conde;
		textIdentificador.setText(vo.getIdentificador()+"");
		textNome.setText(vo.getNome());
		textEmail.setText(vo.getEmail());
		textNickName.setText(vo.getNickname());
		textPassword.setText(vo.getPassword());
		textPasswordConfir.setText(vo.getPassword());
		selectAno.setSelectedIndex(DataConde.getIndex(vo.getNascimento().getYear()));
		selctMes.setSelectedIndex(vo.getNascimento().getMonth());
		selectDia.setSelectedIndex(vo.getNascimento().getDate());
		selectDia.setEnabled(false);
		if(vo.getSexo().equals(Sexo.MASCULINO)) {
			radioMasculino.setSelected(true);
		}
		else {
			radioFeminino.setSelected(true);
		}
		block();
		buttonNovo.setEnabled(true);
		buttonBuscar.setEnabled(true);
		buttonSalvar.setEnabled(false);
		ButtonAlterar.setEnabled(true);
			
	}
	
	/*
	 *Este Metodo faz a chamada para pesquisar uma pessoa por nickname no banco de dados
	 */
	@Override
	public void pesquisarString(String nickname) {
		Pessoa pessoa = PessoaDAO.getPessoaNickName(nickname);
		preencher(pessoa);
		estado = StateView.COM_INFORMACAO_BLOQUEADO;
	}

	@Override
	public void pesquisarID(Long id) {
		
	}
	
	/*
	 * Posiciona os elementos na tela
	 */
	
	@Override
	protected void modelarLayout() {
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(labelConfirPassword, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelNickName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelnascimento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelSexo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(labelPassword)
                                    .addGap(95, 95, 95)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(buttonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(selectAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(selctMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(selectDia, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(textPassword)
                                                .addComponent(textPasswordConfir, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                                                .addComponent(textNome)
                                                .addComponent(textEmail)
                                                .addComponent(textNickName))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(radioMasculino)
                                        .addGap(18, 18, 18)
                                        .addComponent(radioFeminino, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonCancelar))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63)
                        .addComponent(textIdentificador)))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelIdentificador)
                    .addComponent(textIdentificador))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNome)
                    .addComponent(textNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEmail)
                    .addComponent(textEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelNickName)
                    .addComponent(textNickName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelnascimento)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(selectDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(selctMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(selectAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPassword)
                    .addComponent(textPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textPasswordConfir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelConfirPassword))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelSexo)
                    .addComponent(radioFeminino)
                    .addComponent(radioMasculino))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ButtonAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonCancelar))
                .addContainerGap(34, Short.MAX_VALUE))
            );
	}

}
