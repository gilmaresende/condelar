package com.condelar.vision.base;

import javax.swing.JPanel;

public abstract class CondePanelBase extends JPanel implements CondePanelView {

	protected abstract void modelarLayout();
}
