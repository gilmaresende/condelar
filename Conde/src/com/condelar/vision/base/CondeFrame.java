package com.condelar.vision.base;

import java.awt.Toolkit;

import javax.swing.JFrame;
public class CondeFrame extends JFrame{

	public void centralizarComponente() { 
		java.awt.Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
		java.awt.Dimension dw = this.getSize(); setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2); 
	}
}
