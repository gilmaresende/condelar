package com.condelar.vision.base;

import com.condelar.model.CondeModel;

public interface CondePanelView {
	
	public void pesquisarString(String id);
	public void pesquisarID(Long id);
	public void preencher(CondeModel vo);
}
