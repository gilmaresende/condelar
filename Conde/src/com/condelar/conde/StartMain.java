package com.condelar.conde;

import java.util.List;

import javax.swing.JFrame;

import com.condelar.constants.ConstantsConde;
import com.condelar.vision.*;

public class StartMain {

	public static void main(String[] args) {
		
		CondeRunFrame p = new CondeRunFrame();
		p.start();
		
	}

}
