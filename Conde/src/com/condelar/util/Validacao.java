package com.condelar.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validacao {
	
	
	public static boolean validarEmail(String email) {
		
		Pattern pattern = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");   
        Matcher matcher = pattern.matcher(email);   
        return matcher.find();   
	}

}
