package com.condelar.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;
public class DataConde extends Date{
	
	public DataConde(int year, int month, int day) {
		super(year,month,day);
	}
	
	public static int quantidadeDiasMes(int mes, int ano) {
		if (mes == 2) {
			if(ano%4==0) {
				return 29;
			}
			return 28;
		} else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
			return 30;
		} else {
			return 31;
		}
	}

	public static String[] getYearListString(int fistYear, int lastYear) {
		
		String list[] = new String[lastYear-fistYear+2];
		list[0] = "Ano";
		for (int i = 1, j = fistYear; i < lastYear-fistYear+2; i++, j++) {
			list[i] = j+"";
		}
		return list;
	}
	
	public static int getYear(int index) {
		
		Integer list[] = new Integer[152];
		list[0] = 0;
		for (int i = 1, j = 1900; i < 151; i++, j++) {
			list[i] = j;
		}
		return list[index];
	}
	
	public static int getIndex(int year) {
		int r = 0;
		for (int i = 1, j = 1900; i < 151; i++, j++) {
			if(j==year) {
				r = i;
				break;
			}
		}
		return r;
	}
	
	public static int getYearWIndex(int index) {
		int r = 0;
		for (int i = 1, j = 1900; i < 151; i++, j++) {
			if(i==index) {
				r = j;
				break;
			}
		}
		return r;
	}
	
	public static int getDayToday(){
		Calendar c = new GregorianCalendar();
		return  c.get(Calendar.DATE); 
	}
	
	public static int getMonthToday(){
		Calendar c = new GregorianCalendar();
		return  c.get(Calendar.MONTH)+1; 
	}
	
	
	public static int getYearToday(){
		Calendar c = new GregorianCalendar();
		return  c.get(Calendar.YEAR); 
	}

}
